<?php

/**
 -------------------------------------------------------------------------
 CentralFAQ plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/centralfaq
 -------------------------------------------------------------------------

 LICENSE

 This file is part of CentralFAQ.

 CentralFAQ is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 CentralFAQ is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with CentralFAQ. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginCentralfaqConfig extends PluginConfigVariable {

   const VAR_DEFAULT_PAGE = 'default_page';
   const PLUGIN           = 'centralfaq';

   //static $rightname      = 'plugin_centralfaq_config';
   protected $displaylist = false;

   static function getTypeName($nb = 0) {
      return PluginCentralfaqTr::__('Страница FAQ на главной');
   }

   static function getMenuName() {
      return PluginCentralfaqTr::__('Страница FAQ на главной');
   }

   function showFormMain() {

      echo '<tr class="tab_bg_2">';
      echo '<td>' . PluginCentralfaqTr::__('Страница FAQ по умолчанию') . '</td>';
      echo '<td>';
      echo '<input type="hidden" name="name" value="' . self::VAR_DEFAULT_PAGE . '">';
      echo '<input type="hidden" name="plugin" value="' .  self::PLUGIN . '">';
      $opt = [
         'display' => false,
         'value' => '',
         'name' => 'value',
      ];
      if (isset($this->fields['value'])) {
         $opt['value'] = $this->fields['value'];
      }
      echo Dropdown::show('KnowbaseItem', $opt);
      echo '</td>';
      echo '</tr>';

   }

}
