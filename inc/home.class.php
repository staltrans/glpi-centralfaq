<?php

/**
 -------------------------------------------------------------------------
 CentralFAQ plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/centralfaq
 -------------------------------------------------------------------------

 LICENSE

 This file is part of CentralFAQ.

 CentralFAQ is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 CentralFAQ is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with CentralFAQ. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginCentralfaqHome extends CommonGLPI {

   const VIEW_CONFIG = 1024;

   static $rightname = 'plugin_centralfaq_home';

   function getTabNameForItem(CommonGLPI $item, $withtemplate = 0) {
      if (!$withtemplate && $item->getType() == 'Central' && self::canView()) {
         return __('Help');
      }
      return '';
   }

   static function displayTabContentForItem(CommonGLPI $item, $tabnum = 1, $withtemplate = 0) {
      if ($item->getType() == 'Central' && self::canView()) {
         $config = new PluginCentralfaqConfig();
         $kb_item = new KnowbaseItem();
         $default_page = $config->get($config::VAR_DEFAULT_PAGE);
         if ($kb_item->getFromDB($default_page)) {
            if (KnowbaseItemTranslation::canBeTranslated($kb_item)) {
               $content = '<h1>' . KnowbaseItemTranslation::getTranslatedValue($kb_item, 'name') . '</h1>';
               $content .= KnowbaseItemTranslation::getTranslatedValue($kb_item, 'answer');
            } else {
               $content = '<h1>' . $kb_item->fields['name'] . '</h1>';
               $content .= $kb_item->fields['answer'];
            }
            echo '<div id="kbanswer" class="left">';
            echo Toolbox::unclean_html_cross_side_scripting_deep($content);
            echo '</div>';
         }
      }
      return true;
   }

   static function canView() {
      return Session::haveRight(self::$rightname, self::VIEW_CONFIG);
   }

   function getRights($interface = 'central') {
      return [
         self::VIEW_CONFIG => __('View'),
      ];
   }

}
