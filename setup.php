<?php

/**
 -------------------------------------------------------------------------
 CentralFAQ plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/centralfaq
 -------------------------------------------------------------------------

 LICENSE

 This file is part of CentralFAQ.

 CentralFAQ is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 CentralFAQ is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with CentralFAQ. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

define('PLUGIN_CENTRALFAQ_VERSION', '0.1');

/**
 * Init hooks of the plugin.
 * REQUIRED
 *
 * @return void
 */
function plugin_init_centralfaq() {
   global $PLUGIN_HOOKS;

   Plugin::registerClass('PluginCentralfaqHome', ['addtabon' => 'Central']);
   Plugin::registerClass('PluginCentralfaqProfile', ['addtabon' => 'Profile']);

   $PLUGIN_HOOKS['menu_toadd']['centralfaq'] = ['plugins' => 'PluginCentralfaqConfig'];
   $PLUGIN_HOOKS['csrf_compliant']['centralfaq'] = true;
}


/**
 * Get the name and the version of the plugin
 * REQUIRED
 *
 * @return array
 */
function plugin_version_centralfaq() {
   return [
      'name'           => 'Central FAQ',
      'version'        => PLUGIN_CENTRALFAQ_VERSION,
      'author'         => '<a href="https://bitbucket.org/staltrans/">StalTrans</a>',
      'license'        => 'GPLv3',
      'homepage'       => 'https://bitbucket.org/staltrans/glpi-centralfaq',
      'minGlpiVersion' => '9.2'
   ];
}

/**
 * Check pre-requisites before install
 * OPTIONNAL, but recommanded
 *
 * @return boolean
 */
function plugin_centralfaq_check_prerequisites() {
   // Strict version check (could be less strict, or could allow various version)
   $plugin = new Plugin();
   if (!$plugin->isInstalled('config')) {
      printf(__('This plugin requires %s', 'centralfaq'), '<a href="https://bitbucket.org/staltrans/glpi-config">Config GLPI plugin</a>');
      return false;
   }
   if (!$plugin->isActivated('config')) {
      printf(__('Please activate %s', 'centralfaq'), '<a href="https://bitbucket.org/staltrans/glpi-config">Config GLPI plugin</a>');
      return false;
   }
   if (version_compare(GLPI_VERSION, '9.2', 'lt')) {
      if (method_exists('Plugin', 'messageIncompatible')) {
         echo Plugin::messageIncompatible('core', '9.2');
      } else {
         printf(__('This plugin requires GLPI >= %', 'centralfaq'), '9.2');
      }
      return false;
   }
   return true;
}

/**
 * Check configuration process
 *
 * @param boolean $verbose Whether to display message on failure. Defaults to false
 *
 * @return boolean
 */
function plugin_centralfaq_check_config($verbose = false) {
   if (true) { // Your configuration check
      return true;
   }

   if ($verbose) {
      _e('Installed / not configured', 'centralfaq');
   }
   return false;
}
